package pe.gob.zee.zeeauthorization;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ZeeAuthorizationApplicationTests {

	@Test
	void contextLoads() {
		String str1 = "abc";
		String str2 = "abc";
		assertEquals(str1, str2);
	}

}
