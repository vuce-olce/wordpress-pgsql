package pe.gob.zee.zeeauthorization.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.gob.zee.zeeauthorization.bean.ToRecord;
import pe.gob.zee.zeeauthorization.model.Response;
import java.util.Calendar;
import java.util.Random;

@RestController
@CrossOrigin
@RequestMapping("/v1/zona-services/security")
@Slf4j
public class TrainingController {

    /**
     * Método para leer comprobación.
     * @return
     */
    @GetMapping("/health_check")
    public @ResponseBody
    ResponseEntity<Response> healthCheck() {

        ToRecord toRecord = new ToRecord();
        toRecord.setResponse(new Response());
        toRecord.getResponse().setHttStatusCode(HttpStatus.OK.value());
        toRecord.getResponse().setCode(HttpStatus.OK.value());
        toRecord.getResponse().setDescription("health_check (2):" + getRandomByYear());

        log.info(toRecord.getResponse().toString());

        return new ResponseEntity<Response>(toRecord.getResponse(), HttpStatus.valueOf(toRecord.getResponse().getHttStatusCode()));
    }

    /**
     * Método para leer saludo.
     * @return
     */
    @GetMapping("/saludo")
    public String saludo () {
        return "testing";
    }

    private String getRandomByYear() {
        Calendar cal = Calendar.getInstance();
        int nYear = cal.get(Calendar.YEAR);
        Random numero = new Random();
        int randomNum = numero.nextInt(9990000) + 1000000;
        return String.valueOf(nYear) + Integer.toString(randomNum);
    }
}
