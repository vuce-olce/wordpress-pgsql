package pe.gob.zee.zeeauthorization.model;

import java.io.Serializable;

/**
 * Clase Response
 * @author leonardo torres (leonardotorres@adiera.com)
 * @version 1.0.0
 */
public class Response implements Serializable {
	
  private Integer httStatusCode;
  private Integer code;
  private String description;
  private IData data;
	    
    public Integer getHttStatusCode() {
        return httStatusCode;
    }

    public void setHttStatusCode(Integer httStatusCode) {
        this.httStatusCode = httStatusCode;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public IData getData() {
        return data;
    }

    public void setData(IData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Response {\n");

        sb.append("    httStatusCode: ").append(toIndentedString(httStatusCode)).append("\n");
        sb.append("    code: ").append(toIndentedString(code)).append("\n");
        sb.append("    description: ").append(toIndentedString(description)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
