package pe.gob.zee.zeeauthorization.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pe.gob.zee.zeeauthorization.properties.AppProperties;
import pe.gob.sunat.tecnologia.menu.bean.UsuarioBean;
import java.util.Base64;
import java.io.Serializable;
import org.springframework.util.SerializationUtils;
import java.nio.charset.StandardCharsets;

@Controller
@CrossOrigin
@RequestMapping("/v1/zona-services/security")
public class AuthorizationController {
    @Autowired
    private AppProperties properties;

    /**
     * Método para leer sesión Usuario.
     * @param sesionId
     * @param t
     * @param solId
     * @param model
     * @return
     */
    @GetMapping("/sessions")
    public String sessions(
            @RequestParam("sesion_id") String sesionId,
            @RequestParam("t") String t,
            @RequestParam("sol_id") String solId,
            Model model
    ) {
        model.addAttribute("ui_url", this.properties.getUrl());
        model.addAttribute("sesion_id", sesionId);

        byte[] base64User = solId.getBytes(StandardCharsets.UTF_8);
        byte[] usuarioBeanDecoded = Base64.getDecoder().decode(base64User);

        UsuarioBean usuarioBean = (UsuarioBean) SerializationUtils.deserialize(usuarioBeanDecoded);
        model.addAttribute("usuario_bean", usuarioBean.toString());
        //return new ModelAndView("sessions");
        return "sessions";
    }
}
