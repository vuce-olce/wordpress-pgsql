package pe.gob.zee.zeeauthorization.bean;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import pe.gob.zee.zeeauthorization.model.IData;
import pe.gob.zee.zeeauthorization.model.Response;

@Getter
@Setter
public class ToRecord implements Serializable, IData {
    private Response response;
}
