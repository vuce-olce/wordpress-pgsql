package pe.gob.zee.zeeauthorization.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "zee.ui")
@Getter
@Setter
public class AppProperties {

    private String url;
}
