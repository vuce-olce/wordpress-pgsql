package pe.gob.zee.zeeauthorization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZeeAuthorizationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZeeAuthorizationApplication.class, args);
	}

}
