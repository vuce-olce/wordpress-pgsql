<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wpdata' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'secret' );

/** Database hostname */
define( 'DB_HOST', '172.18.0.4' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'cC`G9N9XKCCM@=g~*jeAxv?AW)LzeBX:y]KJPpd?;~FY{z/L)rsVOh8I|{4y~I8_' );
define( 'SECURE_AUTH_KEY',  ',wet`Hf1&Ag$iXfxmrOF`q=~z5&49m~{_~)NUQ]nWH|v96$VPy12)?}aRXtZ-)Zp' );
define( 'LOGGED_IN_KEY',    'RA*J>t6Au@@zS`b)jo1F5tsSLi_r IS#xEm9G(/FEbs#r]5`=|fej%},Ri?`1G0x' );
define( 'NONCE_KEY',        ';.54RgwA32TndL>vK4@[4ZJ8CaVM` Q>-Cp5qG8[+M7vQMQQ4t%WqcoZYz=LsI?I' );
define( 'AUTH_SALT',        'S[;[p-%9}zjU?)3Y!hoZ0Y`{$-/_h3A{5Ng}{.fJt;,%0n5~O=9w%Cf^3OweJp=z' );
define( 'SECURE_AUTH_SALT', '2s3_@H{HD[S=[VB&e$n$GClj@TzmV6*wGu(%d:*)_<(Dux4J?/AEkMF``R*)Q}Q5' );
define( 'LOGGED_IN_SALT',   '!YKKhZ4DpIwAI]&#!_gk[OD>wN$(fgy~>Uv7y&[WK5N3`)DJk$Vo#rmGzX:_r,C5' );
define( 'NONCE_SALT',       'zrUc)!8-vTjfo[V7|3w^#oD[}>MN}52S<pWcZw^VTmr<JW3vy*,Tg`A3{`adE#Ao' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
