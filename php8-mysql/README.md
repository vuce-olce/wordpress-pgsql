# Instalación Wordpress con PostgreSQL

## 1. Configuración de la red

```bash
$ docker network create --subnet 172.18.0.0/16 --gateway 172.18.0.1 net-olce
```
```bash
## 2. Instalación y Configuración de Mysql
# Creación de un volumen persistente para almacenamiento de las bases de datos
$ docker volume create mysql-db-data

# Iniciar el contenedor con la imagen oficial de mysql
$ docker run -d --ip 172.18.0.4 --add-host mysql-host:172.18.0.4 -p 3306:3306 --net net-olce --name mysql-db  -e MYSQL_ROOT_PASSWORD=secret --mount src=mysql-db-data,dst=/var/lib/mysql mysql

# esperar que inice el servidor 2 minutos y  nos conectamos
$ docker exec -it mysql-db mysql -p

# Creamos  base de datos a usar
mysql> create database wpdata;


## 3. Instalación y Configuración de Wordpress

```

### 3.2 Creación de la imagen docker

Editar el archivo src/wp-config.php con los valores de conexión a la base de datos creada en el paso anterior

```php
// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wpdata' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'secret' );

/** Database hostname */
define( 'DB_HOST', '172.18.0.4' );
```

Crear la imagen docker desde el archivo Dockerfile
```
$ docker build -t wp-alpine .
```

### 3.3 Inicialización de Wordpress

Inicializamos el contenedor con la imagen creada en el paso anterior

```bash
# Iniciar el contenedor con la imagen descargada
$ docker run -d --ip 172.18.0.5 --net net-olce --name wordpress -p 80:80 wp-alpine
```

### 3.4 Instalación de Wordpress

Accedemos al URL del sitio wordpress http://olce.innovate.pe

Ingresamos siguiente información:
- Site Title: OLCE
- Username: admin
- Password: ****
- Your Email: ****

A continuación hacemos click en "Install WordPress"

![instalación de wordpress](img/wp-1.png)

Ignoramos los errores (se deben a que por ser la primera instalación, las tablas en postgres necesitan ser creadas), y damos click en "Log In"

![instalación de wordpress](img/wp-2.png)

Ingresmos el usuario y clave para acceder al consola de administración

![instalación de wordpress](img/wp-3.png)

A continuación se muestra la consola de administración

![instalación de wordpress](img/wp-4.png)